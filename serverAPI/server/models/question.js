const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const questionShema = new Schema({
  userID: { type: Schema.Types.ObjectId, ref: "User" },
  pitanje: String,
  datatime: String,
  kategorija: String
});

module.exports = mongoose.model("Question",questionShema,"Question");
