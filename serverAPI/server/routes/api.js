const express = require("express");
const config = require("config");
const jwt = require("jsonwebtoken");
const router = express.Router();
const User = require("../models/user");
const Question = require("../models/question");
const Answer = require("../models/answer");
const monguse = require("mongoose");

monguse.connect(
  config.get("Customer.dbConfig").dbAddress,
  { useNewUrlParser: true },
  err => {
    if (err) {
      console.log("Error connect to database " + err);
    } else {
      console.log("Connected to mongodb");
    }
  }
);

function verifyToken(req, res, next) {
  if (!req.headers.authorization) {
    return res.status(401).send("Unauthorized request");
  }
  let token = req.headers.authorization.split(" ")[1];
  if (token === "null") {
    return res.status(401).send("Unauthorized request");
  }

  let payload = jwt.verify(token, "secretKey");

  if (!payload) {
    return res.status(401).send("Unauthorized request");
  }
  req.userId = payload.subject;
  next();
}



router.post("/login", (req, res) => {
  let userData = req.body;
  User.findOne({ username: userData.username }, (err, user) => {
    if (err) {
      console.log(err);
    } else {
      if (!user) {
        res.status(401).send({message:"Invalid username"});
      } else if (user.password !== userData.password) {
        res.status(401).send({message:"Invalid password"});
      } else {
        let payload = { 
          userID: user._id,
          username: user.username
         };
        res.status(200).send(payload);
      }
    }
  });
});

//QUESTION-------------------------------------------------------------------------------------------------------------

//Vrati podatke 
router.get("/question", (req, res) => {
  console.log(req.query);
  console.log(req.body);
  console.log(req.params);
  console.log(req);
  Question.find(req.query)
  .populate('userID')
  .exec(function(err, question) {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(question);
    }
  });
});//radi


router.get('/question/:questionId',(req,res)=>{
  let questionId = req.params.questionId;
  Question.findById(questionId, function(err, myquestions) {
    if(err) {
      console.log(err);
    } else {
      console.log(myquestions);
      res.status(200).send(myquestions);
    }
  });
});//radi


router.put("/question/:id", (req, res) => {
  let questionId = req.params.id;
  Question.findByIdAndUpdate(
    questionId,
    req.body,
    { upsert: true, useFindAndModify: false },
    (err, response) => {
      if (err) {
        console.log(err);
      } else {
        res.status(200).send({ message: "Updated!" });
      }
    }
  );
});//radi


router.post('/question',(req,res)=>{
  let questionData = req.body;
  let question = new Question(questionData);
  question.save((err, newQuestion) => {
    if (err) {
      console.log(err);
    } else {
      console.log(newQuestion);
      res.status(200).send(newQuestion);
    }
  });
});//radi


router.delete("/question/:id", (req, res) => {
  let questionIdForDelete = req.params.id;
  console.log(questionIdForDelete);
  Question.findByIdAndDelete(questionIdForDelete, (err, response) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).json({ message: "Deleted!" });
    }
  });
});


//ANSWER------------------------------------------------------------------------------------------------------------


router.get('/answer',(req,res)=>{
  //console.log(Answer.find());
  Answer.find().exec(function(err,answer){
    if(err) {
      console.log(err);
    } else {
      console.log(answer);
      res.status(200).send(answer);
    }
  });
});//radi


router.post('/answer', (req ,res) => {
  let answerData = req.body;
  let answer = new Answer(answerData);
  console.log("answer post");
  console.log(answer);
  answer.save((err, newAnswer) => {
    if (err) {
      console.log(err);
    } else {
      console.log(newAnswer);
      res.status(200).send(newAnswer);
    }
  });
});



router.put("/answer/:id",  (req, res) => {
  let userId = req.params.id;
   Answer.findByIdAndUpdate(
    userId,
    req.body,
    { upsert: true, useFindAndModify: false },
    (err, response) => {
      if (err) {
        console.log(err);
      } else {
        res.status(200).send({ message: "Updated!" });
      }
    }
  );
});//radi


router.delete("/answer/:id", (req, res) => {
  let answerIdForDelete = req.params.id;
  Answer.findByIdAndDelete(answerIdForDelete, (err, response) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).json({ message: "Deleted! " });
    }
  });
});//radi


//USER-----------------------------------------------------------------------------------------------------------


router.get("/users",(req,res)=>{
    User.find().exec(function(err,user){
      if(err) {
        console.log(err);
      } else {
        res.status(200).send(user);
      }
    });
});



router.put("/users/:id", (req, res) => {
  let userId = req.params.id;
  User.findByIdAndUpdate(
    userId,
    req.body,
    { upsert: true, useFindAndModify: false },
    (err,response)=>{
      if(err){
        console.log(err);
      } else {
        console.log(response);
        res.status(200).send({message:"Updated!"});
      }
    }
  );
});



router.post("/register", (req, res) => {
  let userData = req.body;
  let user = new User(userData);
  console.log(req);
  user.save((err, registerUser) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(registerUser);
    }
  });
});




module.exports = router;
