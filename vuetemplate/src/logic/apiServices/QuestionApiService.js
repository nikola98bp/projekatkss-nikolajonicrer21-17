import baseUrlWithoutForwardSlash from "./config/baseUrl";
import axios from "axios";

export default class QuestionApiService {
    axios;

    constructor() {
        this.axios = axios;
    }

    getList(serchParams) {
        if(serchParams) {
            return this.axios.get(`${baseUrlWithoutForwardSlash}/question`, {
                params: {
                    ...serchParams
                }
            });
        }
        return this.axios.get(`${baseUrlWithoutForwardSlash}/question`);
    }

    get(questionId) {
        return this.axios.get(`${baseUrlWithoutForwardSlash}/question/${questionId}`);
    }

    post(questionInsertDto) {
        return this.axios.post(`${baseUrlWithoutForwardSlash}/question/`, { ...questionInsertDto });
    }

    put(questionId, questionUpdateDto) {
        return this.axios.put(`${baseUrlWithoutForwardSlash}/question/${questionId}`, { ...questionUpdateDto});
    }

    delete(questionId) {
        return this.axios.delete(`${baseUrlWithoutForwardSlash}/question/${questionId}`);
    }
}