import baseUrlWithoutForwardSlash from "./config/baseUrl";
import axios from "axios";

export default class UserApiService {
    axios;
    constructor(){
        this.axios = axios;
    }
    
    loginPost(loginDto){
        return this.axios.post(`${baseUrlWithoutForwardSlash}/login`, { ...loginDto });
    }

    post(userInsertDto){
        return this.axios.post(`${baseUrlWithoutForwardSlash}/register`, { ...userInsertDto });
    }

    put(userId, userUpdateDto){
        return this.axios.put(`${baseUrlWithoutForwardSlash}/users/${userId}`,{ ...userUpdateDto });
    }
}