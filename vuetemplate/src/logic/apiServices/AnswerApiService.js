import baseUrlWithoutForwardSlash from "./config/baseUrl";
import axios from "axios";

export default class AnswerApiService {
    axios;

    constructor() {
        this.axios = axios;
    }

    getList(serchParams) {
        if(serchParams) {
            return this.axios.get(`${baseUrlWithoutForwardSlash}/answer`, {
                params: {
                    ...serchParams
                }
            });
        }
        return this.axios.get(`${baseUrlWithoutForwardSlash}/answer`);
    }

    get(answerId) {
        return this.axios.get(`${baseUrlWithoutForwardSlash}/answer/${answerId}`);
    }

    post(answerInsertDto) {
        return this.axios.post(`${baseUrlWithoutForwardSlash}/answer`, { ...answerInsertDto });
    }

    put(answerId, answerUpdateDto) {
        return this.axios.put(`${baseUrlWithoutForwardSlash}/answer/${answerId}`, { ...answerUpdateDto});
    }

    delete(answerId) {
        return this.axios.delete(`${baseUrlWithoutForwardSlash}/Answer/${answerId}`);
    }
}