import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/questions',
    name: 'questions',
    component: () => import( '../views/Questions.vue')
  },
  {
    path: '/asksomething',
    name: 'asksomething',
    component: () => import( '../views/AskSomething.vue')
  },
  {
    path: '/settings',
    name: 'settings',
    component: () => import( '../views/Settings.vue')
  }/*,
  {
    path: '/login',
    name: 'login',
    component: () => import( '../views/Login.vue')
  }*/
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
/*
router.beforeEach(from,to, df) {
  next()
}*/

export default router
 
 /*{
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import( '../views/About.vue')
  }*/
 